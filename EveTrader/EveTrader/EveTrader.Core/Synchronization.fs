﻿namespace EveTrader.Domain.Synchronization


open System
open EveTrader.Core.Database
open EveTrader.Core.Logging
open EveTrader.Core.HttpExtensions

type SynchronizationError =
    NetworkError of HttpRequestError
    | DatabaseError of exn


module SynchronizationError =
    let mapNetworkError (error: HttpRequestError) = NetworkError(error)
    let mapDatabaseError (error: exn) = DatabaseError(error)
    

module Region =
    open EveTrader.Domain
    open EveTrader.Utils
    open EveTrader.Domain.Esi
    open EveTrader.Core.HttpExtensions

    let queryRegions (): AsyncResult<list<Region.Root>, HttpRequestError> =
        async {
            let mutable result = []
            let! regionsResult = Esi.Regions.query()
            match regionsResult with
            | Ok(regions) ->
                for regionId in regions |> Seq.map RegionId.ofInt do
                    let! region = regionId |> Esi.Region.query
                    result <- region :: result
                return result |> ResultExtensions.reduce
            | Error(error) -> return Error(error)
        }

    [<Literal>]
    let private ``Insert Region`` ="""
            INSERT INTO 
                eve_trader.region(region_id, name, description) 
            VALUES 
                (@regionId, @name, @description) 
            ON CONFLICT 
                (region_id) DO NOTHING"""

    let updateRegionsInDb (settings: Settings) (regions: list<Esi.Region.Root>): AsyncResult<int, exn> = 
        async { 
            let mutable rowSum = 0
            use dbAccess = new DatabaseAccess(settings) in
                let conn, trans = dbAccess.Get()
                use insertCommand = EveTraderDatabase'.CreateCommand<``Insert Region``>(conn, trans, settings.CommandTimeOut)
                try
                    for region in regions do
                        let! numOfRowsUpdated = insertCommand.AsyncExecute(region.RegionId, region.Name, String.Empty)
                        rowSum <- rowSum + numOfRowsUpdated
                    return Ok(rowSum)
                with
                    exn -> 
                        logger.Error(exn, "Failed to update regions in database")
                        return (Error(exn))
        }

    let synchronizeRegions (updateRegions: list<Esi.Region.Root> -> AsyncResult<int, exn>): AsyncResult<int, SynchronizationError> =
        queryRegions()
        |> AsyncResult.mapError SynchronizationError.mapNetworkError
        |> AsyncResult.bind (updateRegions >> AsyncResult.mapError SynchronizationError.mapDatabaseError)

module Constellation =
    open EveTrader.Domain
    open EveTrader.Core.HttpExtensions
    open EveTrader.Utils

    type ConstellationIds = ConstellationId []

    let queryConstellations (settings: Settings):AsyncResult<array<ConstellationIds>, HttpRequestError> =
        async {
            let! constellationsResult = Esi.Constellations.query()
            match constellationsResult with
            | Ok(constellations) ->
                return constellations 
                        |> Seq.map ConstellationId.ofInt
                        |> Seq.chunkBySize settings.ParralelBatchSize
                        |> Array.ofSeq
                        |> Ok
            | Error(error) -> return Error(error)
        }

    [<Literal>]
    let private ``Insert Constellation`` = 
            """
            INSERT INTO 
                eve_trader.constellation(constellation_id, name, region_id) 
            VALUES 
                (@constellationId, @name, @regionId) 
            ON CONFLICT 
                (constellation_id) DO NOTHING"""

    let updateConstellationsInDb (settings: Settings) (constellation: Esi.Constellation.Root): AsyncResult<int, exn> = 
        async { 
            use dbAccess = new DatabaseAccess(settings) in
                let conn, trans = dbAccess.Get()
                use insertCommand = EveTraderDatabase'.CreateCommand<``Insert Constellation``>(conn, trans, settings.CommandTimeOut)
                try
                    let! affectedRowSum = insertCommand.AsyncExecute(constellation.ConstellationId, constellation.Name, constellation.RegionId)
                    return Ok(affectedRowSum)
                with
                    exn -> 
                        logger.Error(exn, "Failed to update constellations in database")
                        return (Error(exn))
        }

    let synchronizeConstellations (settings: Settings) (updateConstellation: Esi.Constellation.Root -> AsyncResult<int, exn>): AsyncResult<int, SynchronizationError> =
        let synchronizeConstellation (constellationId: ConstellationId): AsyncResult<int, SynchronizationError> =
            async {
                let! constellationResponse = Esi.Constellation.query constellationId
                match constellationResponse with
                | Ok(constellation) -> 
                        let! updateResult = updateConstellation constellation
                        return updateResult |> Result.mapError SynchronizationError.mapDatabaseError
                | Error(error) -> return Error(error |> SynchronizationError.mapNetworkError)
            }

        let sumIfOk = ResultExtensions.combine (+)

        async {
            let! constellationsResult = queryConstellations settings
            match constellationsResult with
            | Ok(constellations) ->
                let mutable affectedRowsResult = Ok(0)
                for (idx, constellationBatch) in constellations |> Seq.indexed do
                    logger.Information (sprintf "Updating constellation batch #%d/%d" idx (constellations.Length - 1))

                    let! updateOperationResults = constellationBatch |> Seq.map synchronizeConstellation |> Async.Parallel
                    let batchResult = updateOperationResults |> Array.fold sumIfOk (Ok(0))
                    affectedRowsResult <- sumIfOk batchResult affectedRowsResult
                return affectedRowsResult

            | Error(x) -> return x |> SynchronizationError.mapNetworkError |> Error
        }

module SolarSystem =
    open EveTrader.Domain
    open EveTrader.Core.HttpExtensions
    open EveTrader.Utils

    type SolarSystemIds = array<SolarSystemId>

    let querySystems (settings: Settings): AsyncResult<array<SolarSystemIds>, HttpRequestError> =
        async {
            let! systemssResult = Esi.SolarSystems.query()
            match systemssResult with
            | Ok(systems) ->
                let result = 
                    systems 
                    |> Seq.map SolarSystemId.ofInt 
                    |> Seq.chunkBySize settings.ParralelBatchSize
                    |> Array.ofSeq
                return Ok(result)
            | Error(error) -> return Error(error)
        }

    [<Literal>]
    let private ``Insert Solar system`` =
            """
            INSERT INTO 
                eve_trader.system(system_id, name, constellation_id, security_class, security_status) 
            VALUES 
                (@systemId, @name, @constellationId, @securityClass, @securityStatus) 
            ON CONFLICT 
                (system_id) DO NOTHING"""

    let updateSystemInDb (settings: Settings) (system: Esi.SolarSystem.Root): AsyncResult<int, exn> = 
        async { 
            use dbAccess = new DatabaseAccess(settings) in
                let conn, trans = dbAccess.Get()
                use insertCommand = EveTraderDatabase'.CreateCommand<``Insert Solar system``>(conn, trans, settings.CommandTimeOut)
                try
                    let! affectedRowSum =  insertCommand.AsyncExecute(system.SystemId, system.Name, system.ConstellationId, system.SecurityClass, double system.SecurityStatus)
                    return Ok(affectedRowSum)
                with
                    exn -> 
                        logger.Error(exn, "Failed to update solar system in database")
                        return (Error(exn))
        }


    let synchronizeSystems (settings: Settings) (updateSystem: Esi.SolarSystem.Root -> AsyncResult<int, exn>): AsyncResult<int, SynchronizationError> =
        let synchronizeSystem (systemId: SolarSystemId): AsyncResult<int, SynchronizationError> =
            async {
                let! systemResponse = Esi.SolarSystem.query systemId
                match systemResponse with
                | Ok(system) -> let! updateResult = updateSystem system
                                return updateResult |> Result.mapError SynchronizationError.mapDatabaseError
                | Error(err) -> return Error(err |> SynchronizationError.mapNetworkError)
            }

        let sumIfOk = ResultExtensions.combine (+)

        async {
            let! systemsResult = querySystems(settings)
            match systemsResult with
            | Ok(systems) ->
                let mutable affectedRowsResult = Ok(0)
                for (idx, systemBatch) in systems |> Seq.indexed do

                    logger.Information (sprintf "Updating system batch #%d/%d" idx (systems.Length-1))
                    let! updateOperationResults = systemBatch |> Seq.map synchronizeSystem |> Async.Parallel
                    let batchResult = updateOperationResults |> Array.fold sumIfOk (Ok(0))
                    affectedRowsResult <- sumIfOk batchResult affectedRowsResult
                return affectedRowsResult

            | Error(x) -> return x |> SynchronizationError.mapNetworkError |> Error
        }

module Type =
    open EveTrader.Domain
    open EveTrader.Domain.Esi
    open EveTrader.Utils

    let queryTypeIds (settings: Settings): AsyncResult<list<list<TypeId>>, HttpRequestError> =
        let queryTypesInPages: AsyncResult<list<int>, HttpRequestError> =
            async {
                let! typesPageSumResult = Types.queryPageSum()

                match typesPageSumResult with
                | Ok(typesPageSum) ->
                    let mutable result: list<Result<array<int>, HttpRequestError>> = []
                    let pages =
                        [1..typesPageSum] 
                        |> Seq.map Page.ofInt 
                        |> Seq.choose ResultExtensions.isOk

                    for page in pages do
                        let! typesInPageResult = page |> Types.query
                        result <- typesInPageResult :: result

                    return result 
                            |> ResultExtensions.reduce 
                            |> Result.map (List.collect List.ofArray)
                | Error(err) -> return Error(err)
            }

        async {
            let! typeIds = queryTypesInPages
            return typeIds 
                    |> Result.map ((List.map TypeId.ofInt) >> (List.chunkBySize settings.ParralelBatchSize))
        }
    
    [<Literal>]
    let private ``Insert Type`` = """
                INSERT INTO
                    eve_trader.type (
                    type_id,
                    name,
                    published,
                    market_group_id,
                    volume,
                    packaged_volume,
                    group_id)
                VALUES
                    (@typeId,
                    @name,
                    @published,
                    @marketGroupId,
                    @volume,
                    @packagedVolume,
                    @groupId)
                ON CONFLICT
                    DO NOTHING"""

    [<Literal>]
    let private ``Insert Type without market group id`` = """
                INSERT INTO
                    eve_trader.type (
                    type_id,
                    name,
                    published,
                    volume,
                    packaged_volume,
                    group_id)
                VALUES
                    (@typeId,
                    @name,
                    @published,
                    @volume,
                    @packagedVolume,
                    @groupId)
                ON CONFLICT
                    DO NOTHING"""

    let updateTypeInDb (settings: Settings) (type': Type.Root): AsyncResult<int, exn> =
        async {
            use dbAccess = new DatabaseAccess(settings) in
                let conn, trans = dbAccess.Get()
                use insertCommand = 
                    EveTraderDatabase'.CreateCommand<``Insert Type``>(conn, trans, settings.CommandTimeOut)

                use insertWithoutMarketGroupIdCommand = 
                    EveTraderDatabase'.CreateCommand<``Insert Type without market group id``>(conn, trans, settings.CommandTimeOut)
                try
                    match type'.MarketGroupId with
                    | Some(marketGroupId) ->
                        let! res = insertCommand.AsyncExecute(type'.TypeId, type'.Name, type'.Published, marketGroupId, type'.Volume,
                                    type'.PackagedVolume, type'.GroupId)
                        return Ok(res)
                    | None -> 
                        let! res = insertWithoutMarketGroupIdCommand.AsyncExecute(type'.TypeId, type'.Name, type'.Published, type'.Volume,
                                    type'.PackagedVolume, type'.GroupId)
                        return Ok(res)
                with
                    exn -> 
                        logger.Error(exn, "Failed to update type in database")
                        return Error(exn)
        } 

    let synchronizeTypes (settings: Settings) (updateType: Type.Root -> AsyncResult<int, exn>): AsyncResult<int, SynchronizationError> =
        let synchronizeType (typeId: TypeId): AsyncResult<int, SynchronizationError> =
            async {
                let! queriedTypeResult = Type.query typeId
                match queriedTypeResult with
                | Ok(queriedType) -> let! result = updateType queriedType
                                     return result |> Result.mapError SynchronizationError.mapDatabaseError
                | Error(x) -> return Error(x |> SynchronizationError.mapNetworkError)
            }
            
        let sumIfOk = ResultExtensions.combine (+)

        async {
            let! typeIdsResult = queryTypeIds settings
            match typeIdsResult with
            | Ok(typeIds) ->
                    let mutable affectedRowsResult = Ok(0)
                    for (idx, batchOfIds) in typeIds |> Seq.indexed do
                        logger.Information (sprintf "Updating Type batch %d/%d" idx (typeIds.Length-1))

                        let! updateOperationResults = batchOfIds |> Seq.map synchronizeType |> Async.Parallel
                        let batchResult = updateOperationResults |> Array.fold sumIfOk (Ok(0))
                        affectedRowsResult <- sumIfOk batchResult affectedRowsResult
                    return affectedRowsResult
            | Error(err) -> return Error(err |> SynchronizationError.mapNetworkError)
        }

module Order =
    open EveTrader.Domain
    open EveTrader.Domain.Esi
    open EveTrader.Utils
    open CachedTransaction

    let queryRegions (settings: Settings): AsyncResult<seq<RegionId>, exn> =
        async {
            use dbAccess = new DatabaseAccess(settings) in
                let conn, trans = dbAccess.Get()
                use selectCommand = EveTraderDatabase'.CreateCommand<"""SELECT region_id FROM eve_trader.region""">(conn, trans, settings.CommandTimeOut)
                try
                    let! regionIdsRaw = selectCommand.AsyncExecute()
                    return regionIdsRaw |> Array.ofSeq |> Seq.map RegionId.ofInt |> Ok
                with
                    error -> return Error(error)
        }

    [<Literal>]
    let private ``Select order by id`` = 
            """
            SELECT *
            FROM
                eve_trader.order
            WHERE order_id = @orderId
            FOR
                NO KEY UPDATE"""

    [<Literal>]
    let private ``Insert order`` =
                    """
                    INSERT INTO
                        eve_trader.order (
                            order_id, 
                            type_id, 
                            price, 
                            is_buy, 
                            volume_total, 
                            volume_remaining, 
                            min_volume,
                            range, 
                            location_id, 
                            system_id, 
                            duration, 
                            issued)
                    VALUES 
                        (@orderId, 
                        @typeId, 
                        @price, 
                        @isBuy, 
                        @volumeTotal, 
                        @volumeRemaining, 
                        @minVolume,
                        @range, 
                        @locationId, 
                        @systemId, 
                        @duration, 
                        @issued)"""

    [<Literal>]
    let private ``Update order by id`` =
                    """
                    UPDATE
                        eve_trader.order
                    SET
                        price = @price, 
                        volume_total = @volumeTotal, 
                        volume_remaining = @volumeRemaining, 
                        range = @range, 
                        duration = @duration
                    WHERE
                        order_id = @orderId"""

    let updateOrdersInDb (settings: Settings) (orders: seq<Esi.Orders.Root>) : AsyncResult<int, exn> =
        async {
            try
                use dbAccess = new DatabaseAccess(settings) in
                    let conn, trans = dbAccess.Get()
                    let mutable affectedSum = 0
                    use selectCommand = EveTraderDatabase'.CreateCommand<``Select order by id``, SingleRow = true>(conn, trans, settings.CommandTimeOut)
                    use insertCommand = EveTraderDatabase'.CreateCommand<``Insert order``>(conn, trans, settings.CommandTimeOut)
                    use updateCommand = EveTraderDatabase'.CreateCommand<``Update order by id``>(conn, trans, settings.CommandTimeOut)
                    for order in orders do
                        let duration = order.Duration |> float |> TimeSpan.FromDays
                        let! maybeRecord = selectCommand.AsyncExecute(order.OrderId)
                        match maybeRecord with
                        | Some(row) -> 
                            let mutable hasChanged = false
                            if row.price <> order.Price then
                                hasChanged <- true
                            if row.duration <> duration then
                                hasChanged <- true
                            if row.range <> order.Range then
                                hasChanged <- true
                            if row.volume_remaining <> order.VolumeRemain then
                                hasChanged <- true
                            if row.volume_total <> order.VolumeTotal then
                                hasChanged <- true
                            if hasChanged then
                                let! affected = updateCommand.AsyncExecute(order.Price, order.VolumeTotal, order.VolumeRemain, order.Range, duration, order.OrderId) 
                                affectedSum <- affected + affectedSum
                        | None -> let! affected = insertCommand.AsyncExecute(order.OrderId, order.TypeId, order.Price, order.IsBuyOrder, 
                                                        order.VolumeTotal, order.VolumeRemain, order.MinVolume, order.Range, order.LocationId, 
                                                        order.SystemId, duration, order.Issued)
                                  affectedSum <- affected + affectedSum
                return Ok(affectedSum)
            with
                error -> 
                    logger.Error(error, "Failed to update orders in database")
                    return Error(error)
        }

    [<Literal>]
    let private ``Select all order ids in region`` = """SELECT order_id FROM eve_trader.order_details WHERE region_id = @regionId"""

    [<Literal>]
    let private ``Delete order`` = """delete from eve_trader."order" where order_id = @orderId"""

    let cleanupOrdersInRegion (settings: Settings) (activeOrderIds: Set<OrderId>) ((RegionId regionId): RegionId) : AsyncResult<int, exn> =
        async {
            try
                let mutable affectedSum = 0
                use dbAccess = new DatabaseAccess(settings) in
                    let conn, trans = dbAccess.Get()
                    use selectCommand = EveTraderDatabase'.CreateCommand<``Select all order ids in region``>(conn, trans, settings.CommandTimeOut)
                    use deleteCommand = EveTraderDatabase'.CreateCommand<``Delete order``>(conn, trans, settings.CommandTimeOut)
                    let! existingOrdersRaw = selectCommand.AsyncExecute(regionId)
                    let existingOrders = existingOrdersRaw |> Seq.choose id |> Seq.map OrderId.ofLong |> Set.ofSeq
                    let ordersToRemove = activeOrderIds |> Set.difference existingOrders
                    for (OrderId orderId) in ordersToRemove do
                        let! affectedRows = deleteCommand.AsyncExecute(orderId)
                        affectedSum <- affectedSum + affectedRows
                return Ok(affectedSum)
            with
                error -> 
                    logger.Error(error, "Failed to clean up orders in database")
                    return Error(error)
        }
    
    let synchronizeOrdersInRegion (settings: Settings) 
                                    (updateInDb: seq<Esi.Orders.Root> -> AsyncResult<int, exn>) 
                                    (cleanupDb: Set<OrderId> -> RegionId -> AsyncResult<int, exn>) 
                                    (regionId: RegionId): AsyncResult<int, SynchronizationError> = 
        
        let synchronizeOrderChunk (page: Esi.Page): Async<array<Esi.Orders.Root>> =
            async {
                let! ordersResult = regionId |> Esi.Orders.query page
                match ordersResult with
                | Ok(orders) -> 
                        do! orders |> updateInDb |> Async.Ignore; 
                        return orders
                | Error(_) -> return Array.empty
            }

        async {
            let! pageSumResult = regionId |> Esi.Orders.queryPageSum
            let mutable processedOrderIds = Set.empty
            match pageSumResult with
            | Ok(pageSum) ->
                let pageChunks = 
                    [1..pageSum] 
                    |> Seq.map Page.ofInt 
                    |> Seq.choose ResultExtensions.isOk 
                    |> Seq.chunkBySize settings.ParralelBatchSize 
                    |> Seq.indexed
                    |> Array.ofSeq

                for (chunkIdx, pages) in pageChunks do
                    logger.Information (sprintf "Processing order page chunks %d/%d in region: %A" chunkIdx (pageChunks.Length-1) regionId)
                    let! processedOrders = 
                            pages 
                            |> Seq.map synchronizeOrderChunk 
                            |> Async.Parallel
                    
                    let currentlyProcessedIds = 
                            processedOrders 
                            |> Array.collect id 
                            |> Seq.map Esi.Orders.getOrderId
                            |> Set.ofSeq
                    processedOrderIds <- processedOrderIds |> Set.union currentlyProcessedIds
                                            

            | Error(_) -> ()
            let! result = cleanupDb processedOrderIds regionId
            return result |> Result.mapError SynchronizationError.mapDatabaseError
        }

    let synchronizeOrders 
                            (queryRegions: AsyncResult<seq<RegionId>, exn>) 
                            (syncOrdersInRegion: RegionId -> AsyncResult<int, SynchronizationError>)
                            (queryCacheState: string -> AsyncResult<CacheState, HttpRequestError>)
                            (updateCacheState: string -> Etag -> ExpiryDate -> Async<int>)
                            : AsyncResult<int, SynchronizationError> =
        async {
            let! regionsResult = queryRegions
            match regionsResult with
            | Ok(regions) ->
                    for regionId in regions do
                        let innerSynchronizeOrders: AsyncResult<int, SynchronizationError> = regionId |> syncOrdersInRegion
                        let regionUrl = Esi.Orders.getUrl regionId
                        let queryCacheState' = (fun () -> queryCacheState regionUrl)
                        let updateCacheState' = updateCacheState regionUrl
                        do! CachedTransaction.cachedTransaction SynchronizationError.mapNetworkError queryCacheState' updateCacheState' innerSynchronizeOrders  |> Async.Ignore

                    return Ok(0)
            | Error(err) -> return Error(err |> SynchronizationError.mapDatabaseError)
        }