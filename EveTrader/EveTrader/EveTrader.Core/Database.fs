﻿namespace EveTrader.Core

module Database =

    type Settings =
        { ConnString: string; CommandTimeOut: int; ParralelBatchSize: int }

    open FSharp.Data.Npgsql
    open System

    [<Literal>]
    let ConnStringEnvVariable: string = "ConnectionStrings:eve-market"

    [<Literal>]
    let ConnStringEnvVariableShort: string = "eve-market"

    type EveTraderDatabase = NpgsqlConnection<ConnStringEnvVariableShort, ConfigType = ConfigType.Environment>
    type EveTraderDatabase' = NpgsqlConnection<ConnStringEnvVariableShort, ConfigType = ConfigType.Environment, XCtor = true>
    type DbTransactionExecutor<'a> = ((Npgsql.NpgsqlConnection * Npgsql.NpgsqlTransaction) -> Async<'a>) -> Async<'a>

    type DbConnTransactionPair = Npgsql.NpgsqlConnection * Npgsql.NpgsqlTransaction

    type DatabaseAccess(conn: Npgsql.NpgsqlConnection, trans: Npgsql.NpgsqlTransaction) =
        new(settings: Settings) =
            let conn = new Npgsql.NpgsqlConnection(settings.ConnString)
            conn.Open()
            let trans = conn.BeginTransaction()
            new DatabaseAccess(conn, trans)

        member this.Get() = conn, trans

        interface IDisposable with
            member this.Dispose() =
                trans.Commit()
                trans.Dispose()
                conn.Close()
                conn.Dispose()