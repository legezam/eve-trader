namespace EveTrader.Domain.Esi

open FSharp.Data
open EveTrader.Core
module Literals =
    [<Literal>]
    let DataSourceQueryParam: string = "datasource"
    [<Literal>]
    let Tranquility: string = "tranquility"
    [<Literal>]
    let LanguageQueryParam: string = "language"
    [<Literal>]
    let PageQueryParam: string = "page"
    [<Literal>]
    let OrderTypeQueryParam: string = "order_type"
    [<Literal>]
    let TypeIdQueryParam: string = "type_id"
    [<Literal>]
    let AllOrderType: string = "all"
    [<Literal>]
    let EnUs: string = "en-us"
    [<Literal>]
    let PagesHeader: string = "X-Pages"
    
    let FirstPage = sprintf "%d" 1

type Page = Page of int32

module Page =
    let ofInt(page: int32): Result<Page, string> =
        match page with
        | greaterThanZero when greaterThanZero > 0 -> Ok(Page(greaterThanZero))
        | _ -> Error "Page cannot be less than 1"

    let getNumberOfPages (response: HttpResponse): int32 =
            response.Headers.TryFind Literals.PagesHeader
            |> Option.map int32 
            |> Option.defaultValue 0

module Constellations =
    open EveTrader.Core.HttpExtensions
    type Response = JsonProvider<"../Responses/constellations.response.json">

    let url: string = "https://esi.evetech.net/latest/universe/constellations/"

    let query (): Async<Result<int[], HttpRequestError>> =
        async {
            let! rawResponse = url |> HttpExtensions.retryRequestWithQuery [(Literals.DataSourceQueryParam, Literals.Tranquility)]
            return rawResponse |> Result.map (HttpExtensions.extractBodyAsText >> Response.Parse)
        }

module Constellation =
    open EveTrader.Domain
    open EveTrader.Core.HttpExtensions
    open EveTrader.Utils

    type Response = JsonProvider<"../Responses/constellation.response.json">

    type Root = Response.Root

    let getUrl ((ConstellationId constellationId): ConstellationId): string = 
            sprintf "https://esi.evetech.net/latest/universe/constellations/%d/" constellationId

    let query (constellationId: ConstellationId): AsyncResult<Root, HttpRequestError> = 
        async { 
            let! rawResponse = constellationId 
                                |> getUrl 
                                |> HttpExtensions.retryRequestWithQuery [(Literals.DataSourceQueryParam, Literals.Tranquility)
                                                                         (Literals.LanguageQueryParam, Literals.EnUs)]
            return rawResponse |> Result.map (HttpExtensions.extractBodyAsText >> Response.Parse)
        }

module SolarSystems =
    open EveTrader.Core.HttpExtensions

    type Response = JsonProvider<"../Responses/systems.response.json">

    let url: string = "https://esi.evetech.net/latest/universe/systems/"

    let query (): Async<Result<int[], HttpRequestError>> =
        async {
            let! rawResponse = url |> HttpExtensions.retryRequestWithQuery [(Literals.DataSourceQueryParam, Literals.Tranquility)]
            return rawResponse |> Result.map (HttpExtensions.extractBodyAsText >> Response.Parse)
        }

module SolarSystem =
    open EveTrader.Domain
    open EveTrader.Core.HttpExtensions

    type Response = JsonProvider<"../Responses/system.response.json">

    type Root = Response.Root

    let getUrl ((SolarSystemId solarSystemId) : SolarSystemId): string = sprintf "https://esi.evetech.net/latest/universe/systems/%d/" solarSystemId

    let query (solarSystemId: SolarSystemId): Async<Result<Root, HttpRequestError>> = 
        async { 
            let! rawResponse = solarSystemId 
                                |> getUrl 
                                |> HttpExtensions.retryRequestWithQuery [(Literals.DataSourceQueryParam, Literals.Tranquility)
                                                                         (Literals.LanguageQueryParam, Literals.EnUs)]
            return rawResponse |> Result.map (HttpExtensions.extractBodyAsText >> Response.Parse)
        }

module Regions =
    open EveTrader.Core.HttpExtensions

    type Response = JsonProvider<"../Responses/regions.response.json">
    
    let url: string = "https://esi.evetech.net/latest/universe/regions/"

    let query (): Async<Result<int[], HttpRequestError>> =
        async { 
            let! rawResponse = url |> HttpExtensions.retryRequestWithQuery [ (Literals.DataSourceQueryParam, Literals.Tranquility)]
            return rawResponse |> Result.map (HttpExtensions.extractBodyAsText >> Response.Parse)
        }

module Region =
    open EveTrader.Domain
    open EveTrader.Core.HttpExtensions

    type Response = JsonProvider<"../Responses/region.response.json">

    type Root = Response.Root

    let getUrl ((RegionId id): RegionId): string = sprintf "https://esi.evetech.net/latest/universe/regions/%d" id
    
    let query (regionId: RegionId): Async<Result<Root, HttpRequestError>> = 
        async { 
            let! rawResponse = regionId 
                                |> getUrl 
                                |> HttpExtensions.retryRequestWithQuery [(Literals.DataSourceQueryParam, Literals.Tranquility)
                                                                         (Literals.LanguageQueryParam, Literals.EnUs)]
            return rawResponse |> Result.map (HttpExtensions.extractBodyAsText >> Response.Parse)
        }
    
module Types =
    open EveTrader.Core.HttpExtensions

    type Response = JsonProvider<"../Responses/types.response.json">
    
    let url: string = "https://esi.evetech.net/latest/universe/types/"

    let query ((Page page): Page): Async<Result<int[], HttpRequestError>> =
        async {
            let! rawResponse = url 
                                |> HttpExtensions.retryRequestWithQuery 
                                    [ (Literals.DataSourceQueryParam, Literals.Tranquility)
                                      (Literals.PageQueryParam, sprintf "%d" page) ]

            return rawResponse |> Result.map (HttpExtensions.extractBodyAsText >> Response.Parse)
        }

    let queryPageSum (): Async<Result<int, HttpRequestError>> =
        async {
            let! rawResponse = url 
                                |> HttpExtensions.retryRequestWithQuery 
                                        [ (Literals.DataSourceQueryParam, Literals.Tranquility)
                                          (Literals.PageQueryParam, Literals.FirstPage) ]
            return rawResponse |> Result.map Page.getNumberOfPages
        }
        
module Type =
    open EveTrader.Domain
    open EveTrader.Core.HttpExtensions

    type Response = JsonProvider<"../Responses/type.response.json", SampleIsList=true>

    type Root = Response.Root

    let getUrl ((TypeId id): TypeId): string = sprintf "https://esi.evetech.net/latest/universe/types/%d/" id

    let query (typeId: TypeId): Async<Result<Root, HttpRequestError>> =
        async {
            let! rawResponse = typeId
                                |> getUrl
                                |> HttpExtensions.retryRequestWithQuery 
                                    [ (Literals.DataSourceQueryParam, Literals.Tranquility)
                                      (Literals.LanguageQueryParam, Literals.EnUs) ]
            return rawResponse |> Result.map (HttpExtensions.extractBodyAsText >> Response.Parse)
        }   

module Orders =
    open EveTrader.Domain
    open EveTrader.Core.HttpExtensions

    type Response = JsonProvider<"../Responses/orders.response.json">

    type Root = Response.Root

    let getOrderId(order: Root): OrderId = order.OrderId |> OrderId.ofLong   
    
    let getUrl ((RegionId id): RegionId): string = sprintf "https://esi.evetech.net/latest/markets/%d/orders/" id

    let query ((Page page): Page) (regionId: RegionId): Async<Result<Root [], HttpRequestError>> =
        async {
            let! rawResponse = regionId
                                |> getUrl
                                |> HttpExtensions.retryRequestWithQuery 
                                        [ (Literals.DataSourceQueryParam, Literals.Tranquility)
                                          (Literals.PageQueryParam, sprintf "%d" page)
                                          (Literals.OrderTypeQueryParam, Literals.AllOrderType) ]
            return rawResponse |> Result.map (HttpExtensions.extractBodyAsText >> Response.Parse)
        }
        
    let queryPageSum (regionId: RegionId): Async<Result<int, HttpRequestError>> =
        async {
            let! rawResponse = regionId
                                |> getUrl
                                |> HttpExtensions.retryRequestWithQuery 
                                            [ (Literals.DataSourceQueryParam, Literals.Tranquility)
                                              (Literals.PageQueryParam, Literals.FirstPage)
                                              (Literals.OrderTypeQueryParam, Literals.AllOrderType) ]
            return rawResponse |> Result.map Page.getNumberOfPages
        }