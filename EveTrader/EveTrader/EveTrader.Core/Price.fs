﻿namespace EveTrader.Domain

type Price = private Price of decimal

module Price =
    open System
    open System.Globalization
    open EveTrader.Utils.Comparison
    let get (Price price): decimal = price

    let DefaultMinPrice: Price = Price(0M)
    let DefaultMaxPrice: Price = Price(1_000_000_000_000M)

    let ofDecimal (priceRaw: decimal): Result<Price, string> =
        match priceRaw with
        | price when price >= (DefaultMinPrice |> get) && price <= (DefaultMaxPrice |> get) -> Ok(Price price)
        | _ -> Error "Invalid price. Valid values are between 0 and 1000000000000"

    let ofDecimal2 (priceRaw: decimal): Price = (Price priceRaw)

    let ofString (priceRaw: string): Result<Price, string> =
        let result = Decimal.TryParse(priceRaw, NumberStyles.Number, CultureInfo.InvariantCulture)
        match result with
        | true, value -> ofDecimal value
        | _ -> Error "The provided price value cannot be parsed"

    let compare (Price price) (Price price'): ComparisonResult =
        if price > price' then
            GreaterThan
        else if price = price' then
            Equals
        else
            LesserThan