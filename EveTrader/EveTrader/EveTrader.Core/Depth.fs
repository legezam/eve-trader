﻿namespace EveTrader.Domain

type Depth = private Depth of int32

module Depth =
    open EveTrader.Utils.Comparison
    open System

    let get (Depth depth): int = depth

    let DefaultMaxDepth: Depth = Depth(50)

    let ofInt (depthRaw: int32): Result<Depth, string> =
        match depthRaw with
        | depth when depth >= 0 && depth <= 1000 -> Ok(Depth(depth))
        | _ -> Error "Invalid depth. Valid values are between 0 and 1000"

    let ofString (depthRaw: string): Result<Depth, string> =
        match depthRaw |> Int32.TryParse with
        | true, value -> ofInt value
        | _ -> Error "The provided depth value cannot be parsed"

    let compare (Depth depth) (Depth depth'): ComparisonResult =
        if depth > depth' then
            GreaterThan
        else if depth = depth' then
            Equals
        else
            LesserThan
