﻿namespace EveTrader.Domain


type Order = { price: Price; system: string; region: string; size: Size; sum: Size; securityStatus: Security }

module Order =
    open EveTrader.Core.Database
    open System.Threading.Tasks
    open FSharp.Control.Tasks.V2
    open EveTrader.Utils

    let make (price: Price) (system: string) (region: string) (size: Size) (sum: Size) (securityStatus: Security): Order =
        {
            price = price
            system = system
            region = region
            size = size
            sum = sum
            securityStatus = securityStatus
        }

    [<Literal>]
    let ``Select buy Orders`` = 
        """SELECT 
            price as price,
            volume_remaining as size,
            system_name as systemName,
            region_name as regionName,
            security_status as securityStatus
        FROM 
            eve_trader.order_details 
        WHERE 
            type_id = @typeId AND 
            is_buy is true 
        ORDER BY 
            price DESC"""

    [<Literal>]
    let ``Select sell Orders`` = 
        """SELECT 
            price as price,
            volume_remaining as size,
            system_name as systemName,
            region_name as regionName,
            security_status as securityStatus
        FROM 
            eve_trader.order_details 
        WHERE 
            type_id = @typeId AND 
            is_buy is false 
        ORDER BY 
            price ASC"""

    let queryBuyOrders ((TypeId typeId) : TypeId): Task<seq<Order>> =
        task {
            use selectCommand = EveTraderDatabase.CreateCommand<``Select buy Orders``>(ConnStringEnvVariableShort)
            let! records = selectCommand.AsyncExecute(typeId) |> Async.StartAsTask
            return
                records
                |> Seq.map 
                    (fun row -> 
                        let maybePrice = row.price |> Option.map Price.ofDecimal2
                        let maybeSystem = row.systemname
                        let maybeRegion = row.regionname
                        let maybeSize = row.size |> Option.map Size.ofLong2
                        let maybeSum = 0L |> Size.ofLong2 |> Some
                        let maybeSecStatus = row.securitystatus |> Option.map Security.ofFloat2

                        maybePrice
                        |> Option.map make
                        |> OptionExtensions.tie maybeSystem
                        |> OptionExtensions.tie maybeRegion
                        |> OptionExtensions.tie maybeSize
                        |> OptionExtensions.tie maybeSum
                        |> OptionExtensions.tie maybeSecStatus)
                |> Seq.choose id       
        }
    

    let querySellOrders ((TypeId typeId) : TypeId): Task<seq<Order>> =
        task {
            use selectCommand = EveTraderDatabase.CreateCommand<``Select sell Orders``>(ConnStringEnvVariableShort)
            let! records = selectCommand.AsyncExecute(typeId) |> Async.StartAsTask
            return 
                records
                |> Seq.map (fun row -> 
                                let maybePrice = row.price |> Option.map Price.ofDecimal2
                                let maybeSystem = row.systemname
                                let maybeRegion = row.regionname
                                let maybeSize = row.size |> Option.map Size.ofLong2
                                let maybeSum = 0L |> Size.ofLong2 |> Some
                                let maybeSecStatus = row.securitystatus |> Option.map Security.ofFloat2

                                maybePrice
                                |> Option.map make
                                |> OptionExtensions.tie maybeSystem
                                |> OptionExtensions.tie maybeRegion
                                |> OptionExtensions.tie maybeSize
                                |> OptionExtensions.tie maybeSum
                                |> OptionExtensions.tie maybeSecStatus)
                |> Seq.choose id
        }

    let enrichOrderWithSum (sum: Size) (order: Order): Order * Size = 
        let sum' = Size.sum sum order.size
        let order' = { order with sum = sum' }
        order', sum'

type OrderFilter = { 
        minSecStatus: Security; 
        maxSecStatus: Security; 
        minPrice: Price; 
        maxPrice: Price; 
        minSize: Size; 
        maxSize: Size; 
        maxDepth: Depth }

module OrderFilter =
    open EveTrader.Utils.Comparison
    open EveTrader.Utils.Operators
    
    let make (minSec: Security) 
             (maxSec: Security) 
             (minPrice: Price) 
             (maxPrice: Price) 
             (minSize: Size) 
             (maxSize: Size) 
             (maxDepth: Depth): OrderFilter =
        {
            minSecStatus = minSec
            maxSecStatus = maxSec
            minPrice = minPrice
            maxPrice = maxPrice
            minSize = minSize
            maxSize = maxSize
            maxDepth = maxDepth
        }

    let minSecStatusFilter (minSecurityStatus: Security) (order : Order): bool = 
        match Security.compare order.securityStatus minSecurityStatus with
        | GreaterThan | Equals -> true
        | LesserThan -> false

    let maxSecStatusFilter (maxSecurityStatus: Security) (order : Order): bool =
        match Security.compare order.securityStatus maxSecurityStatus with
        | LesserThan | Equals -> true
        | GreaterThan -> false

    let minPriceFilter (minPrice: Price) (order: Order): bool =
        match Price.compare order.price minPrice with
        | GreaterThan | Equals -> true
        | LesserThan -> false

    let maxPriceFilter (maxPrice: Price) (order: Order): bool =
        match Price.compare order.price maxPrice with
        | LesserThan | Equals -> true
        | GreaterThan -> false

    let minSizeFilter (minSize: Size) (order: Order): bool =
        match Size.compare order.size minSize with
        | GreaterThan | Equals -> true
        | LesserThan -> false

    let maxSizeFilter (maxSize: Size) (order: Order): bool =
        match Size.compare order.size maxSize with
        | LesserThan | Equals -> true
        | GreaterThan -> false

    let filterOrders (orderFilter: OrderFilter): Order -> bool =
        let minSecStatusFilter = minSecStatusFilter orderFilter.minSecStatus
        let maxSecStatusFilter = maxSecStatusFilter orderFilter.maxSecStatus
        let minPriceFilter = minPriceFilter orderFilter.minPrice
        let maxPriceFilter = maxPriceFilter orderFilter.maxPrice
        let minSizeFilter = minSizeFilter orderFilter.minSize
        let maxSizeFilter = maxSizeFilter orderFilter.maxSize

        minSecStatusFilter 
        >>> maxSecStatusFilter 
        >>> minPriceFilter 
        >>> maxPriceFilter 
        >>> minSizeFilter 
        >>> maxSizeFilter

type LevelOne = { 
    minPrice: Price; 
    lowerQuadrantPrice: Price; 
    maxPrice: Price; 
    upperQuadrantPrice: Price; 
    midPrice: Price; 
    averagePrice: Price; 
    weigthedAveragePrice: Price
    sum: Size
    }

module LevelOne =
    open FSharp.Control.Tasks.V2
    open System.Threading.Tasks
    open EveTrader.Core.Database

    let private DefaultLevelOne: LevelOne =
                    {minPrice = Price.DefaultMinPrice; 
                        lowerQuadrantPrice = Price.DefaultMinPrice; 
                        midPrice = Price.DefaultMinPrice; 
                        upperQuadrantPrice = Price.DefaultMinPrice; 
                        maxPrice = Price.DefaultMinPrice;
                        averagePrice = Price.DefaultMinPrice;
                        weigthedAveragePrice = Price.DefaultMinPrice;
                        sum = Size.DefaultMinSize}

    let make (average: decimal) (averageWeighted: decimal) (sum: decimal) (percentiles: decimal []): LevelOne =
        {
            minPrice = percentiles.[0] |> Price.ofDecimal2
            lowerQuadrantPrice = percentiles.[1] |> Price.ofDecimal2
            midPrice = percentiles.[2] |> Price.ofDecimal2
            upperQuadrantPrice = percentiles.[3] |> Price.ofDecimal2
            maxPrice = percentiles.[4] |> Price.ofDecimal2
            averagePrice = average |> Price.ofDecimal2
            weigthedAveragePrice= averageWeighted |> Price.ofDecimal2
            sum = sum |> int64 |> Size.ofLong2
        }
    
    
    [<Literal>]
    let ``Select order price percentiles`` =
        """
        select 
            percentile_disc(array[0,0.25,0.5,0.75,1]) within group (order by price asc) as percentiles,
            avg(price) as averagePrice,
            sum(price * volume_remaining)/sum(volume_remaining) as averagePriceWeighted,
            sum(volume_remaining) as sum
        from eve_trader.order_details
        where is_buy = @isBuy
            and type_id = @typeId
            and security_status >= @minSecurity
            and security_status <= @maxSecurity
        group by type_id"""

    let query (isBuy: bool) (filter: OrderFilter) (typeId: TypeId): Task<LevelOne> = 
        task {
            let typeId = typeId |> TypeId.get
            let minSecurity = filter.minSecStatus |> Security.get
            let maxSecurity = filter.maxSecStatus |> Security.get
            use percentileCommand = EveTraderDatabase.CreateCommand<``Select order price percentiles``, SingleRow=true>(ConnStringEnvVariableShort)
            let! percentileResult = percentileCommand.AsyncExecute(isBuy, typeId, minSecurity, maxSecurity) |> Async.StartAsTask
            return  percentileResult 
                    |> Option.map (fun record -> make (record.averageprice |> Option.get) (record.averagepriceweighted |> Option.get) (record.sum |> Option.get) (record.percentiles |> Option.get)) 
                    |> Option.defaultValue DefaultLevelOne
        }
        
