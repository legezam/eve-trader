﻿namespace EveTrader.Utils

module Operators =
    let (>>>) (firstPred: 'a -> bool) (secondPred: 'a -> bool) (input: 'a): bool =
        if firstPred input then
            secondPred input
        else
            false

    let constant (input: 'a) () = input
    let constant2 (input: 'a) (_: 'b) = input

module OptionExtensions =
    // Applicative <*>
    let tie (value: Option<'a>) (mapper: Option<'a -> 'b>): Option<'b> =
        match value, mapper with
        | Some(value'), Some(mapper') -> Some(mapper' value')
        | _ -> None

module ResultExtensions =
    
    let isOk (result: Result<_, _>): Option<_> =
        match result with
        | Ok(x) -> Some(x)
        | Error(_) -> None

    let isError (result: Result<_, _>): bool =
        match result with
        | Ok(_) -> true
        | Error(_) -> false

    let reduce (input: list<Result<'a, 'b>>): Result<list<'a>, 'b> =
        let acc state itemResult = 
            match state with
            | Error(err) -> Error(err)
            | Ok(list) ->
                match itemResult with
                | Ok(item) -> Ok(item :: list)
                | Error(err) -> Error(err)

        input |> List.fold acc (Ok([]))

    let combine (operation: 'a -> 'a -> 'a) (stateResult: Result<'a, 'b>) (itemResult: Result<'a, 'b>): Result<'a, 'b> =
            match stateResult, itemResult with
            | Ok(state), Ok(item) -> Ok(operation state item)
            | Error(_) as x, _ | _, (Error(_) as x) -> x
        
    // Applicative <*>
    let tie (value: Result<'a, 'b>) (mapper: Result<('a -> 'c), 'b>): Result<'c, 'b> =
        match value, mapper with
        | Ok(innerValue), Ok(innerMapper) -> Ok(innerMapper innerValue)
        | Error(x), _ -> Error(x)
        | _, Error(x) -> Error(x)

type AsyncResult<'a, 'b> = Async<Result<'a, 'b>>

module AsyncResult =

    let mapError (mapper: 'b -> 'c) (input: AsyncResult<'a, 'b>) : AsyncResult<'a, 'c> =
        async {
            let! tmp = input
            match tmp with
            | Ok(x) -> return Ok(x)
            | Error(err) -> return Error(err |> mapper)
        }

    let bind (mapper: 'a -> AsyncResult<'c, 'b>) (input: AsyncResult<'a, 'b>): AsyncResult<'c, 'b> =
        
        async {
            let! result = input
            match result with
            | Ok(x) -> return! x |> mapper
            | Error(err) -> return Error(err)
        }

    let wrap (value: 'a) : AsyncResult<'a, 'b> =
        async {
            return Ok(value)
        }

module StringExtensions =
    let getLength (input: string): int = input.Length

module Comparison =
    type ComparisonResult =
        LesserThan
        | Equals
        | GreaterThan