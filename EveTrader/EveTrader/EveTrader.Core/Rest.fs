﻿namespace EveTrader.Core
open Logging
open FSharp.Data

module Literals =
    [<Literal>]
    let HTTP_GET: string = "GET"
    
    let AcceptJson: string * string = ("accept", "application/json")

    let UserAgent: string * string = ("User-Agent", "eve-trader")

    let ErrorLimitResetHeader: string = "X-Esi-Error-Limit-Reset"

module HttpExtensions =
    open System.Net
    open System

    [<Literal>]
    let TooManyErrorsStatusCode: int = 420

    [<Literal>]
    let MaxNumberOfAttempts = 3

    [<Literal>]
    let RequestTimeout = 30_000

    [<RequireQualifiedAccess>]
    type HttpRequestError =
            TooManyAttempts
            | Unexpected of int
            | Exception of exn

    type AsyncRestResult = Async<Result<HttpResponse, HttpRequestError>>

    let extractErrorLimitResetTime (response: HttpResponse) : TimeSpan =
        response.Headers
        |> Map.tryFind Literals.ErrorLimitResetHeader
        |> Option.map float
        |> Option.defaultValue 0.0
        |> TimeSpan.FromSeconds

    let retryRequestWithHeaderAndQuery (headers: list<string * string>) (query: list<string * string>) (url: string): AsyncRestResult =
        let headers = headers @ [Literals.AcceptJson; Literals.UserAgent]

        let rec inner (attempt: int) =
                async {
                    try
                        let! response = Http.AsyncRequest(url, query = query, headers = headers, silentHttpErrors = true, timeout=RequestTimeout)
                        match response.StatusCode with
                        | HttpStatusCodes.OK
                        | HttpStatusCodes.NotModified -> return Ok(response)
                        | HttpStatusCodes.BadGateway -> return! handleBadGateway attempt
                        | HttpStatusCodes.ServiceUnavailable -> return handleServiceUnavailable()
                        | TooManyErrorsStatusCode -> return! response |> handleErrorLimitBreached attempt
                        | statusCode -> return! statusCode |> handleUnexpectedStatusCode attempt
                    with
                        | :? System.TimeoutException as ex -> return! ex |> handleTimeOutError attempt
                        | error -> return! error |> handleUnexpectedError
                }
        and handleBadGateway (attempt: int): AsyncRestResult =
            async {
                if attempt < MaxNumberOfAttempts then
                    
                    logger.Warning("Bad gateway trying again: {Url}", url)
                    return! inner (attempt+1)
                else
                    return HttpRequestError.TooManyAttempts |> Error
            }
        and handleErrorLimitBreached (attempt: int) (response: HttpResponse): AsyncRestResult =
            async {
                if attempt < MaxNumberOfAttempts then
                    let waitDuration = extractErrorLimitResetTime response
                    logger.Warning("Too many errors: {Url}, retrying in {Seconds} seconds", url, (waitDuration.TotalSeconds))

                    do! waitDuration.TotalMilliseconds 
                        |> int 
                        |> Async.Sleep
                    return! inner (attempt + 1)
                else
                    return HttpRequestError.TooManyAttempts |> Error
            }
        and handleUnexpectedStatusCode (attempt: int) (statusCode: int): AsyncRestResult =
            async {
                if attempt < MaxNumberOfAttempts then
                    logger.Warning("Unexpected status code: {StatusCode}, trying again: {Url}",  statusCode, url)
                    return! inner (attempt+1)
                else
                    return Error (HttpRequestError.Unexpected statusCode)
            }
        and handleTimeOutError (attempt: int) (error: TimeoutException): AsyncRestResult =
            async {
                if attempt < MaxNumberOfAttempts then
                    logger.Warning("Timeout occurred, trying again: {Url}", url)
                    return! inner (attempt+1)
                else
                    return Error(HttpRequestError.Exception error)
            }
        and handleUnexpectedError (error: exn): AsyncRestResult = 
            async {
                    logger.Error("Unexpected error: {Message}, {Url}", error.Message, url)
                    return Error (HttpRequestError.Exception error)
            }
        and handleServiceUnavailable () =
            let err = (InvalidOperationException("ESI is down"))
            raise err
            Error (HttpRequestError.Exception err)
        
        
        inner 0

    let retryRequestWithHeader header = retryRequestWithHeaderAndQuery header []

    let retryRequestWithQuery query = retryRequestWithHeaderAndQuery [] query

    let retryRequest = retryRequestWithHeaderAndQuery [] []

    let convertBodyToText (body: HttpResponseBody): string =
            match body with
            | Text(txt) -> txt
            | Binary(data) -> System.Text.Encoding.Default.GetString(data)
    
    let extractBodyAsText (response: HttpResponse): string = convertBodyToText response.Body


