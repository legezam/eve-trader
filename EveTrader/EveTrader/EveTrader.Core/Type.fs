﻿namespace EveTrader.Domain

type TypeId = TypeId of int

module TypeId =
    let public ofInt(id: int) = TypeId id
    let get (TypeId typeId): int = typeId

module Type =
    open EveTrader.Core.Database
    open EveTrader.Utils
    open FSharp.Control.Tasks.V2
    open System.Threading.Tasks

    type Type = { typeId: TypeId; name: string }

    let queryTypeByName (name: string): Task<Option<Type>> =
        task {
            use selectCommand = EveTraderDatabase.CreateCommand<"""SELECT type_id as typeId, name FROM eve_trader.type WHERE name = @name""", SingleRow=true>(ConnStringEnvVariableShort)
            let! res = selectCommand.AsyncExecute(name) 
            return res |> Option.map (fun record -> { typeId = record.typeid |> TypeId.ofInt; name = record.name })
        }

    let queryTypeByTypeId ((TypeId typeId): TypeId): Task<Option<Type>> =
        task {
            use selectCommand = EveTraderDatabase.CreateCommand<"""SELECT type_id as typeId, name FROM eve_trader.type WHERE type_id = @typeId""", SingleRow=true>(ConnStringEnvVariableShort)
            let! res = selectCommand.AsyncExecute(typeId)
            return res|> Option.map (fun record -> { typeId = record.typeid |> TypeId.ofInt; name = record.name })
        }

    let queryTypeNamesById (typeIds: seq<TypeId>): Task<seq<string>> =
        task {
            use selectCommand = EveTraderDatabase.CreateCommand<"""SELECT name FROM eve_trader.type WHERE type_id = @typeId""", SingleRow=true>(ConnStringEnvVariableShort)
            let result = ResizeArray()
            for (TypeId typeId) in typeIds do
                let! singleResult = selectCommand.AsyncExecute(typeId) |> Async.StartAsTask
                result.Add(singleResult)
            return result
                    |> Seq.choose id
                    |> Seq.sortBy StringExtensions.getLength
        }