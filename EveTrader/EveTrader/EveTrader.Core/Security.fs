﻿namespace EveTrader.Domain

type Security = private Security of float

module Security =
    open System
    open EveTrader.Utils.Comparison
    open System.Globalization

    let get (Security security): float = security

    let DefaultMinSecurity: Security = Security(-1.0)

    let DefaultMaxSecurity: Security = Security(1.0)

    let ofFloat (securityRaw: float): Result<Security, string> =
        match securityRaw with
        | sec when sec >= (DefaultMinSecurity |> get) && sec <= (DefaultMaxSecurity |> get) -> Ok(Security sec)
        | _ -> Error ("Invalid security status. Valid values are between -1.0 and 1.0")

    let ofFloat2( securityRaw: float): Security = Security(securityRaw)

    let ofString (securityRaw: string): Result<Security, string> =
        let result = Double.TryParse(securityRaw, NumberStyles.Number, CultureInfo.InvariantCulture)
        match result with
        | true, value -> ofFloat value
        | _ -> Error "The provided security value cannot be parsed"

    let compare (Security security) (Security security'): ComparisonResult =
        if security > security' then
            GreaterThan
        else if security = security' then
            Equals
        else
            LesserThan
