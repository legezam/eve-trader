﻿namespace EveTrader.Domain

type ConstellationId = ConstellationId of int

module ConstellationId =
    let ofInt (id: int): ConstellationId = ConstellationId id


type SolarSystemId = SolarSystemId of int

module SolarSystemId =
    let ofInt (id: int): SolarSystemId = SolarSystemId id


type RegionId = RegionId of int

module RegionId =
    let ofInt(id: int): RegionId = RegionId id
    let toInt((RegionId id): RegionId): int32 = id


type OrderId = OrderId of int64

module OrderId =
    let ofLong(id: int64): OrderId = OrderId id
    let toLong((OrderId id): OrderId): int64 = id

