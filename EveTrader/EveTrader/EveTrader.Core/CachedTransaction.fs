﻿module CachedTransaction

open FSharp.Data
open System

open EveTrader.Core.Database
open EveTrader.Core.HttpExtensions
open EveTrader.Utils
open EveTrader.Utils.Operators

[<Literal>]
let EtagHeader = "ETag"
[<Literal>]
let IfNoneMatchHeader = "If-None-Match"
[<Literal>]
let ExpiresHeader = "Expires"

type Etag = Etag of string

module Etag =
    let ofString (tagAsString: string): Etag = Etag tagAsString

type ExpiryDate = ExpiryDate of DateTimeOffset

module ExpiryDate =
    let ofString (dateAsString: string): ExpiryDate = dateAsString |> DateTimeOffset.Parse |> ExpiryDate

type CacheState =
    Expired of Etag * ExpiryDate
    | Unchanged

type Url = string

type CachedTransactionEvent =
    Updated
    | NotChanged

let queryCacheState (settings: Settings) (url: string): AsyncResult<CacheState, HttpRequestError> =

    let extractResult (response: HttpResponse): CacheState =
        match response.StatusCode with
        | HttpStatusCodes.NotModified -> Unchanged
        | _ -> 
            let etag = response.Headers.Item EtagHeader |> Etag.ofString
            let expires = response.Headers.Item ExpiresHeader |> ExpiryDate.ofString
            Expired(etag, expires)

    async {
        use dbAccess = new DatabaseAccess(settings) in
            let conn, trans = dbAccess.Get()
            use selectCommand = EveTraderDatabase'.CreateCommand<"""SELECT etag FROM eve_trader.etag where url = @url""", SingleRow = true>(conn, trans, settings.CommandTimeOut)
            let! existingEtag = selectCommand.AsyncExecute(url)
            let! httpResult =
                match existingEtag with
                | Some(existingEtag) ->
                    url |> retryRequestWithHeader [(IfNoneMatchHeader, existingEtag)]
                | None ->
                    url |> retryRequest
        
        return httpResult |> Result.map extractResult
    }

let updateCacheStatus  (settings: Settings) (url: string) ((Etag tagAstring): Etag) ((ExpiryDate expires): ExpiryDate)
            : Async<int> =
    async {
        use dbAccess = new DatabaseAccess(settings) in
            let conn, trans = dbAccess.Get()
            use upsertCommand = EveTraderDatabase'.CreateCommand<"""
                        INSERT INTO 
                            eve_trader.etag(url, etag, expires) 
                        VALUES 
                            (@url, @etag, @expires) 
                        ON CONFLICT (url) 
                            DO UPDATE SET etag = @etag, updated = timezone('utc'::text, now()), expires = @expires""">(conn, trans, settings.CommandTimeOut)
            return! upsertCommand.AsyncExecute(url, tagAstring, expires.UtcDateTime)
    }

let cachedTransaction (httpErrorMapper: HttpRequestError -> 'error) 
                        (queryCacheState: unit -> AsyncResult<CacheState, HttpRequestError>) 
                        (updateCacheStatus: Etag -> ExpiryDate -> Async<int>) 
                        (action: AsyncResult<'a, 'error>)
        : AsyncResult<CachedTransactionEvent, 'error> =
    async {
        let! cacheStatusResult = queryCacheState()
        match cacheStatusResult with
        | Ok(cacheStatus) ->
                match cacheStatus with
                | Expired(etag, expiryDate) -> 
                    let! actionResult = action
                    let! _ = updateCacheStatus etag expiryDate
                    return actionResult |> Result.map (constant2 Updated)
                | Unchanged -> 
                        return Ok(NotChanged)
        | Error(x) -> return Error(x |> httpErrorMapper)
    }

