﻿namespace EveTrader.Domain

type Size = private Size of int64

module Size =
    open System
    open EveTrader.Utils.Comparison

    let get (Size size): int64 = size

    let DefaultMaxSize: Size = Size(1_000_000_000_000L)

    let DefaultMinSize: Size = Size(0L)

    let ofLong (sizeRaw: int64): Result<Size, string> =
        match sizeRaw with
        | size when size >= (DefaultMinSize |> get) && size <= (DefaultMaxSize |> get) -> Ok(Size size)
        | _ -> Error "Invalid size. Valid values are between 0 and 1000000000000"

    let ofLong2 (sizeRaw: int64): Size = Size(sizeRaw)

    let ofString (sizeRaw: string): Result<Size, string> =
        match sizeRaw |> Int64.TryParse with
        | true, value -> ofLong value
        | _ -> Error "The provided size value cannot be parsed"

    let sum (Size size) (Size size'): Size = (size + size') |> ofLong2

    let compare (Size size) (Size size'): ComparisonResult =
        if size > size' then
            GreaterThan
        else if size = size' then
            Equals
        else
            LesserThan