﻿namespace EveTrader.Core

module Logging =
    open Serilog
    open Microsoft.Extensions.Configuration

    let logger: Core.Logger =
        let config = ConfigurationBuilder().AddJsonFile("serilog.json").Build()
        LoggerConfiguration().ReadFrom.Configuration(config).CreateLogger();