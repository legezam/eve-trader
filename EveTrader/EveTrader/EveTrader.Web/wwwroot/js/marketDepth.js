﻿$(function () {
    $("#item").autocomplete({
        minLength: 4,
        source: function(request, response) {
            var term = request.term;
            $.getJSON(
                `https://esi.evetech.net/latest/search/?categories=inventory_type&datasource=tranquility&language=en-us&search=${
                term}`,
                function(searchResult) {
                    $.ajax("/types",
                        {
                            dataType: 'json',
                            success: function(result) {},
                            data: {
                                "typeId": searchResult.inventory_type.join(",")
                            }
                        }).done(function(data) {
                        response(data);
                    });
                });
        }
    });
    $("#item").on("autocompleteselect", function (event, ui) {
        window.location.href = `/type/${ui.item.value}`;

    });
    $("#depth").spinner({
        step: 1,
        min: 0,
        max: 1000,
        numberFormat: "n"
    });
    $("#minSecurity").spinner({
        step: 0.1,
        min: -1.0,
        max: 1.0,
        numberFormat: "n"
    });
    $("#maxSecurity").spinner({
        step: 0.1,
        min: -1.0,
        max: 1.0,
        numberFormat: "n"
    });
    $("#minPrice").spinner({
        step: 1,
        min: 0,
        max: 1000000000000,
        numberFormat: "n"
    });
    $("#maxPrice").spinner({
        step: 1,
        min: 0,
        max: 1000000000000,
        numberFormat: "n"
    });
    $("#minSize").spinner({
        step: 1,
        min: 0,
        max: 1000000000000,
        numberFormat: "n"
    });
    $("#maxSize").spinner({
        step: 1,
        min: 0,
        max: 1000000000000,
        numberFormat: "n"
    });
    $("#filterButton").button();
    $("#filterButton").click(function (event) {
        event.preventDefault();
        var depth = $("#depth").val();
        var minSecurity = $("#minSecurity").val();
        var maxSecurity = $("#maxSecurity").val();
        var minPrice = $("#minPrice").val();
        var maxPrice = $("#maxPrice").val();
        var minSize = $("#minSize").val();
        var maxSize = $("#maxSize").val();
        window.location.href = `?maxDepth=${depth}&minSecurity=${minSecurity}&maxSecurity=${maxSecurity}&minPrice=${minPrice}&maxPrice=${maxPrice}&minSize=${minSize}&maxSize=${maxSize}`;
    });
    $("#highSecButton").button();
    $("#highSecButton").click(function(event) {
        event.preventDefault();
        $("#minSecurity").val(0.5);
        $("#maxSecurity").val(1.0);
    });
    $("#lowSecButton").button();
    $("#lowSecButton").click(function (event) {
        event.preventDefault();
        $("#minSecurity").val(0.1);
        $("#maxSecurity").val(1.0);
    });
    $("#resetButton").button();
    $("#resetButton").click(function(event) {
        event.preventDefault();
        window.location.href = window.location.href.split('?')[0];
    });
    $(".controlgroup-vertical").controlgroup({
        "direction": "vertical"
    });
    $("#showFilterButton").button();
    $("#showFilterButton").click(function(event) {
        event.preventDefault();
        $("#filterGroup").toggle();
    });
    $(document).ready(function() {
        $("#filterGroup").hide();
    });
});

