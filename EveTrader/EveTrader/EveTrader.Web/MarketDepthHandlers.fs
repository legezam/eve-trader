﻿module MarketDepthHandlers

open Microsoft.AspNetCore.Http
open Giraffe
open System
open EveTrader.Domain
open EveTrader.Utils
open FSharp.Data
open FSharp.Control.Tasks.V2 // DO NOT REMOVE THIS. Causes issues with taskbuilders
open System.Threading.Tasks

[<Literal>]
let MinSecurity: string = "minSecurity"
[<Literal>]
let MaxSecurity: string = "maxSecurity"
[<Literal>]
let MinPrice: string = "minPrice"
[<Literal>]
let MaxPrice: string = "maxPrice"
[<Literal>]
let MinSize: string = "minSize"
[<Literal>]
let MaxSize: string = "maxSize"
[<Literal>]
let TypeIdParam: string = "typeId"
[<Literal>]
let MaxDepth: string = "maxDepth"

let QueryListSeparators: char [] = [| ',' |]

let createViewFilter (ctx: HttpContext): Result<OrderFilter, string> =
    
    let minSecStatus = 
            ctx.TryGetQueryStringValue MinSecurity 
            |> Option.map Security.ofString
            |> Option.defaultValue (Security.DefaultMinSecurity |> Ok)
            
            
    let maxSecStatus = 
            ctx.TryGetQueryStringValue MaxSecurity 
            |> Option.map Security.ofString
            |> Option.defaultValue (Security.DefaultMaxSecurity |> Ok)

    let minPrice =
            ctx.TryGetQueryStringValue MinPrice
            |> Option.map Price.ofString
            |> Option.defaultValue (Price.DefaultMinPrice |> Ok)
            
    let maxPrice =
            ctx.TryGetQueryStringValue MaxPrice
            |> Option.map Price.ofString
            |> Option.defaultValue (Price.DefaultMaxPrice |> Ok)
            

    let minSize =
            ctx.TryGetQueryStringValue MinSize
            |> Option.map Size.ofString
            |> Option.defaultValue (Size.DefaultMinSize |> Ok)
            
             
    let maxSize =
            ctx.TryGetQueryStringValue MaxSize
            |> Option.map Size.ofString
            |> Option.defaultValue (Size.DefaultMaxSize |> Ok)
            

    let maxDepth =
            ctx.TryGetQueryStringValue MaxDepth
            |> Option.map Depth.ofString
            |> Option.defaultValue (Depth.DefaultMaxDepth |> Ok)
               
    minSecStatus
        |> Result.map OrderFilter.make
        |> ResultExtensions.tie maxSecStatus
        |> ResultExtensions.tie minPrice
        |> ResultExtensions.tie maxPrice
        |> ResultExtensions.tie minSize
        |> ResultExtensions.tie maxSize
        |> ResultExtensions.tie maxDepth
 
let getTitle (type': Type.Type) = sprintf "%s | Eve Wall Street" type'.name

let renderMarketDepthForType (ctx: HttpContext) (type': Type.Type) (viewFilter: OrderFilter): Task<Option<HttpContext>> =
    task {
        let filterPipeline = viewFilter |> OrderFilter.filterOrders
        let maxDepth = viewFilter.maxDepth |> Depth.get
        let defaultSum = Size.DefaultMinSize

        let! buyOrders =
            type'.typeId 
            |> Order.queryBuyOrders
        let filteredBuyOrders =
            buyOrders
            |> Seq.filter filterPipeline
            |> Seq.mapFold Order.enrichOrderWithSum defaultSum 
            |> fst 
            |> Seq.truncate maxDepth
            |> List.ofSeq
        
        let! sellOrders =
            type'.typeId
            |> Order.querySellOrders 
        let filteredSellOrders =
            sellOrders
            |> Seq.filter filterPipeline
            |> Seq.mapFold Order.enrichOrderWithSum defaultSum 
            |> fst 
            |> Seq.truncate maxDepth
            |> List.ofSeq
        
        let title = type' |> getTitle

        let! buyLevelOne = type'.typeId |> LevelOne.query true viewFilter
        let! sellLevelOne = type'.typeId |> LevelOne.query false viewFilter

        return! [MarketDepthView.typeSearchView type';
                MarketDepthView.viewFilter viewFilter ; 
                MarketDepthView.marketDepthHeaderView type'; 
                MarketDepthView.levelOneView buyLevelOne sellLevelOne;
                MarketDepthView.marketDepthView filteredBuyOrders filteredSellOrders]
                |> MarketDepthView.rootView title
                |> ctx.WriteHtmlViewAsync
    }

let handleViewFilterCreationError (ctx: HttpContext) (message: string): Task<Option<HttpContext>> =
    task {
        ctx.SetStatusCode HttpStatusCodes.BadRequest
        return! ctx.WriteTextAsync message
    }

let getMarketDepthForType (ctx : HttpContext) (type': Type.Type): Task<Option<HttpContext>> =
    let viewFilterResult = ctx |> createViewFilter
    let render = renderMarketDepthForType ctx type'
    let errorHandler = handleViewFilterCreationError ctx
    match viewFilterResult with
    | Ok (viewFilter) -> viewFilter |> render
    | Error(message) ->  message |> errorHandler


let getMarketDepthForTypeName (typeName: string): HttpHandler =
    let handler (_ : HttpFunc) (ctx : HttpContext) =
        task {
            let! maybeType = typeName |> Type.queryTypeByName 
            match maybeType with
            | Some(type') -> return! type' |> getMarketDepthForType ctx
            | _ -> 
                ctx.SetStatusCode HttpStatusCodes.BadRequest
                return! ctx.WriteTextAsync "Type not found"
        }      
    handler

let getMarketDepthForTypeId (typeIdRaw: int): HttpHandler =
    let handler (_: HttpFunc) (ctx: HttpContext) =
        task {
            let typeId = typeIdRaw |> TypeId.ofInt
            let! maybeType = typeId |> Type.queryTypeByTypeId
            match maybeType with
            | Some(type') -> return! type' |> getMarketDepthForType ctx
            | _ -> 
                ctx.SetStatusCode HttpStatusCodes.BadRequest
                return! ctx.WriteTextAsync "Type not found"
        }
    handler

let convertTypeIdsToTypeNames (): HttpHandler =
    let handler (_ : HttpFunc) (ctx : HttpContext) =
        task {
            let maybeTypeIdList = ctx.TryGetQueryStringValue TypeIdParam
            let typeIdList =
                        maybeTypeIdList
                        |> Option.map (fun idlist -> idlist.Split(QueryListSeparators) |> Seq.map int |> Seq.map TypeId.ofInt)
                        |> Option.defaultValue Seq.empty
            let! typeNames = typeIdList |> Type.queryTypeNamesById
            return! typeNames |> ctx.WriteJsonAsync
        }
    handler