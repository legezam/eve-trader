﻿module MarketDepthView

open System
open Giraffe.GiraffeViewEngine
open EveTrader.Domain

[<Literal>]
let Tera: string = "T"
[<Literal>]
let Giga: string = "G"
[<Literal>]
let Mega: string = "M"
[<Literal>]
let Kilo: string = "K"


let getSecStatusClass (secStatus: Security): string =
    let secStatus = secStatus |> Security.get
    match secStatus with
    | status when status = 1.0 -> "sec sec-ten"
    | status when status >= 0.9 -> "sec sec-nine"
    | status when status >= 0.8 -> "sec sec-eight"
    | status when status >= 0.7 -> "sec sec-seven"
    | status when status >= 0.6 -> "sec sec-six"
    | status when status >= 0.5 -> "sec sec-five"
    | status when status >= 0.4 -> "sec sec-four"
    | status when status >= 0.3 -> "sec sec-three"
    | status when status >= 0.2 -> "sec sec-two"
    | status when status >= 0.1 -> "sec sec-one"
    | status when status >= -1.0 -> "sec sec-zero"
    | _ -> String.Empty
       
let formatPriceWithPostFix (postfix: string) (price: Price): string = 
    let price = price |> Price.get
    String.Format("{0:#,0.##} {1}", price, postfix)

let formatPrice = formatPriceWithPostFix String.Empty

let formatSize (size: Size): string = String.Format("{0:#,0}", size |> Size.get)

let formatLocationShort (order: Order): string = sprintf "%s" order.system

let formatLocation (order: Order): string =
    let security = order.securityStatus |> Security.get
    sprintf "%s/%s (%.1f)" order.system order.region security

let formatValueRounded (input: Price): string =
    match input |> Price.get with
    | tera when tera >= 1_000_000_000_000M -> (tera / 1_000_000_000_000M) |> Price.ofDecimal2 |> formatPriceWithPostFix Tera
    | giga when giga >= 1_000_000_000M -> (giga / 1_000_000_000M) |> Price.ofDecimal2 |> formatPriceWithPostFix Giga
    | mega when mega >= 1_000_000M -> (mega / 1_000_000M) |> Price.ofDecimal2 |> formatPriceWithPostFix Mega
    | kilo when kilo >= 1_000M -> (kilo / 1_000M) |> Price.ofDecimal2 |> formatPriceWithPostFix Kilo
    | small -> small |> Price.ofDecimal2 |> formatPriceWithPostFix String.Empty

let formatValueRounded' (input: Size): string =
    let input' =  input |> Size.get |> decimal
    match input' with
    | tera when tera >= 1_000_000_000_000M -> (tera / 1_000_000_000_000M) |> Price.ofDecimal2 |> formatPriceWithPostFix Tera
    | giga when giga >= 1_000_000_000M -> (giga / 1_000_000_000M) |> Price.ofDecimal2 |> formatPriceWithPostFix Giga
    | mega when mega >= 1_000_000M -> (mega / 1_000_000M) |> Price.ofDecimal2 |> formatPriceWithPostFix Mega
    | kilo when kilo >= 1_000M -> (kilo / 1000M) |> Price.ofDecimal2 |> formatPriceWithPostFix Kilo
    | _ -> formatSize input
        

let bidHeader: XmlNode =
        div [ _class "row row-left"] [
            div [ _class "cell row-header cell-left"; _title "Location: Solar system/region"] [ encodedText "Loc" ]
            div [ _class "cell row-header"; _title "Sum: volume sum of orders above the given order"] [ encodedText "Sum" ]
            div [ _class "cell row-header"; _title "Size: volume of order"] [ encodedText "Size" ]
            div [ _class "cell row-header cell-right"; _title "Bid: bid price of order"] [ encodedText "Bid" ]
        ]

let askHeader: XmlNode =
        div [ _class "row row-right"] [
            div [ _class "cell row-header cell-left"; _title "Ask: ask price of order"] [ encodedText "Ask" ]
            div [ _class "cell row-header"; _title "Size: volume of order"] [ encodedText "Size" ]
            div [ _class "cell row-header"; _title "Sum: volume sum of orders above the given order"] [ encodedText "Sum" ]
            div [ _class "cell row-header cell-right"; _title "Location: Solar system/region"] [ encodedText "Loc" ]
        ] 

let buyOrderView (order: Order): XmlNode =
    div [ _class "row row-left bluehover" ] [
        div [ _class "cell cell-left"; _title (formatLocation order) ] [ span [_class (getSecStatusClass order.securityStatus)] [] ; encodedText (formatLocationShort order)]
        div [ _class "cell"; _title (order.sum |> formatSize) ] [ encodedText (order.sum |> formatValueRounded') ]
        div [ _class "cell"; _title (order.size |> formatSize) ] [ encodedText (order.size |> formatValueRounded') ]
        div [ _class "cell cell-right"; _title (order.price |> formatPrice) ] [ encodedText (order.price |> formatValueRounded) ]
    ]

let sellOrderView (order: Order): XmlNode =
    div [ _class "row row-right bluehover" ] [
        div [ _class "cell cell-left"; _title (order.price |> formatPrice) ] [ encodedText (order.price |> formatValueRounded) ]
        div [ _class "cell"; _title (order.size |> formatSize) ] [ encodedText (order.size |> formatValueRounded') ]
        div [ _class "cell"; _title (order.sum |> formatSize) ] [ encodedText (order.sum |> formatValueRounded') ]
        div [ _class "cell cell-right"; _title (formatLocation order) ] [encodedText (formatLocationShort order); span [_class (getSecStatusClass order.securityStatus)] []]
    ]

let buyOrdersView (buyOrders: list<Order>): XmlNode = 
        let orderViews = buyOrders |> List.map buyOrderView
        div [] orderViews

let sellOrdersView (sellOrders: list<Order>): XmlNode = 
        let orderViews = sellOrders |> List.map sellOrderView
        div [] orderViews

let marketDepthView (buyOrders: list<Order>) (sellOrders: list<Order>): XmlNode =
        div [ _class "market-depth" ] [
            div [ _class "column-left bid-bg"] [ 
                bidHeader 
                buyOrdersView buyOrders
            ]
            div [ _class "column-right ask-bg"] [
                askHeader
                sellOrdersView sellOrders
            ]
        ]

let levelOneView (buyLevelOne: LevelOne) (sellLevelOne: LevelOne): XmlNode = 

    div [_class "level-one"] [
        div [ _class "level-one-column-left bid-bg" ] [
            div [_class "level-one-row row-center"] [
                div [_class "level-one-cell"] [
                    div [_title "Min price"] [encodedText "Min"]
                    div [] [encodedText (buyLevelOne.minPrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Lower 25%"] [encodedText "L25%"]
                    div [] [encodedText (buyLevelOne.lowerQuadrantPrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Mid price"] [encodedText "Mid"]
                    div [] [encodedText (buyLevelOne.midPrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [ _title "Avg price"] [encodedText "Avg"]
                    div [] [encodedText (buyLevelOne.averagePrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Weighted Avg price"] [encodedText "WAvg"]
                    div [] [encodedText (buyLevelOne.weigthedAveragePrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Upper 75%"] [encodedText "U75%"]
                    div [] [encodedText (buyLevelOne.upperQuadrantPrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Max price"] [encodedText "Max"]
                    div [] [encodedText (buyLevelOne.maxPrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Sum"] [encodedText "Sum"]
                    div [] [encodedText (buyLevelOne.sum |> formatValueRounded')]
                ]
            ]
        ]
        div [ _class "level-one-column-right ask-bg"] [
            div [_class "level-one-row row-center"] [
                div [_class "level-one-cell"] [
                    div [_title "Min price"] [encodedText "Min"]
                    div [] [encodedText (sellLevelOne.minPrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Lower 25%"] [encodedText "L25%"]
                    div [] [encodedText (sellLevelOne.lowerQuadrantPrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Mid price"] [encodedText "Mid"]
                    div [] [encodedText (sellLevelOne.midPrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [ _title "Avg price"] [encodedText "Avg"]
                    div [] [encodedText (sellLevelOne.averagePrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Weighted Avg price"] [encodedText "WAvg"]
                    div [] [encodedText (sellLevelOne.weigthedAveragePrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Upper 75%"] [encodedText "U75%"]
                    div [] [encodedText (sellLevelOne.upperQuadrantPrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Max price"] [encodedText "Max"]
                    div [] [encodedText (sellLevelOne.maxPrice |> formatValueRounded)]
                ]
                div [_class "level-one-cell"] [
                    div [_title "Sum"] [encodedText "Sum"]
                    div [] [encodedText (sellLevelOne.sum |> formatValueRounded')]
                ]
            ]
        ]
    ]

let marketDepthHeaderView (type' : Type.Type): XmlNode =
    let (TypeId typeId) = type'.typeId
    div [ _class "market-depth-header"] [
        div [] [
            img [ _src (sprintf "https://imageserver.eveonline.com/Type/%d_64.png" typeId); _width "64"; _height "64"]
        ]
        div [] [
            h1 [ _class "market-depth-header-title"] [ encodedText type'.name ]
        ]
    ]

let typeSearchView (type' : Type.Type): XmlNode =
    fieldset [] [
        legend [] [ encodedText "Item selection"]
        div [ _class "controlgroup-vertical" ] [
            label [ _for "item"] [ encodedText "Item: "]
            input [ _id "item"; _class "ui-widget ui-widget-content item-widget"; _value type'.name]
            button [ _id "showFilterButton"; _class "ui-button ui-widget ui-corner-all" ] [encodedText "Show filter"]
        ]
    ]

let viewFilter (filter: OrderFilter): XmlNode =
    fieldset [ _id "filterGroup"] [
        legend [] [ encodedText "Filtering"]
        div [ _class "controlgroup-vertical"] [
                label [ _for "depth" ] [ encodedText "Max depth: "]
                input [ _id "depth"; _value (sprintf "%d" (filter.maxDepth |> Depth.get))]
                label [ _for "minSecurity" ] [ encodedText "Min security: "]
                input [ _id "minSecurity"; _value (sprintf "%f" (filter.minSecStatus |> Security.get))]
                label [ _for "maxSecurity" ] [ encodedText "Max security: "]
                input [ _id "maxSecurity"; _value (sprintf "%f" (filter.maxSecStatus |> Security.get))]
                button [ _id "highSecButton"; _class "ui-button ui-widget ui-corner-all" ] [encodedText "High sec only"]
                button [ _id "lowSecButton"; _class "ui-button ui-widget ui-corner-all" ] [encodedText "Low and high sec only"]
                label [ _for "minPrice" ] [ encodedText "Min price: "]
                input [ _id "minPrice"; _value (sprintf "%f" (filter.minPrice |> Price.get))]
                label [ _for "maxPrice" ] [ encodedText "Max price: "]
                input [ _id "maxPrice"; _value (sprintf "%f" (filter.maxPrice |> Price.get))]
                label [ _for "minSize" ] [ encodedText "Min size: "]
                input [ _id "minSize"; _value (sprintf "%d" (filter.minSize |> Size.get))]
                label [ _for "maxSize" ] [ encodedText "Max size: "]
                input [ _id "maxSize"; _value (sprintf "%d" (filter.maxSize |> Size.get))]
                button [ _id "filterButton"; _class "ui-button ui-widget ui-corner-all" ] [encodedText "Filter"]
                button [ _id "resetButton"; _class "ui-button ui-widget ui-corner-all" ] [encodedText "Reset"]
        ]
    ]

let rootView (title': string) (containerContent: XmlNode list): XmlNode = 
    html [] [
        head [] [
            title [] [rawText title']
            link [ _rel "stylesheet"; _href "https://fonts.googleapis.com/css?family=Roboto" ]
            link [ _rel "stylesheet"; _href "//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"]
            link [ _rel "stylesheet"; _type "text/css"; _href "../css/index.css" ]
            script [ _src "https://code.jquery.com/jquery-1.12.4.js" ] []
            script [ _src "https://code.jquery.com/ui/1.12.1/jquery-ui.js" ] []
            script [ _src "../js/marketDepth.js" ] [ ]
        ]
        body [] [
            div [ _class "container" ] containerContent
        ]
    ]