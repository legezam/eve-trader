﻿open Giraffe
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.DependencyInjection
open Microsoft.AspNetCore.Hosting
open System
open Quartz
open System.Collections.Specialized
open Quartz.Impl
open System.Threading.Tasks
open System.Diagnostics
open EveTrader.Core.Database
open EveTrader.Core.Logging
open EveTrader.Domain
open EveTrader.Domain.Synchronization
open EveTrader.Utils

let mutable Scheduler: option<IScheduler> = None

type Synchronization(settings: Settings) =
    let queryCacheState = CachedTransaction.queryCacheState settings
    let updateCacheState = CachedTransaction.updateCacheStatus settings
    let updateRegions = Region.updateRegionsInDb settings
    let updateConstellations = Constellation.updateConstellationsInDb settings
    let synchronizeConstellations = Constellation.synchronizeConstellations settings
    let updateSystems = SolarSystem.updateSystemInDb settings
    let synchronizeSystems = SolarSystem.synchronizeSystems settings
    let updateTypes = Type.updateTypeInDb settings
    let synchronizeTypes = Type.synchronizeTypes settings
    let updateOrders = Order.updateOrdersInDb settings
    let cleanupOrders = Order.cleanupOrdersInRegion settings
    let synchronizeOrdersInRegion = Order.synchronizeOrdersInRegion settings
    let queryRegions = Order.queryRegions settings

    let cachedTransaction = CachedTransaction.cachedTransaction SynchronizationError.mapNetworkError

    member private this.GetCacheStateOperations(url: string) =
        (fun () -> queryCacheState url), updateCacheState  url

    member this.SynchronizeRegion() = 
        let url = Esi.Regions.url
        let synchronizeRegions = Region.synchronizeRegions updateRegions
        let queryCacheState, updateCacheState = this.GetCacheStateOperations(url)

        cachedTransaction queryCacheState updateCacheState synchronizeRegions

    member this.SynchronizeConstellation() = 
        let url = Esi.Constellations.url
        let synchronizeConstellations = synchronizeConstellations updateConstellations
        let queryCacheState, updateCacheState = this.GetCacheStateOperations(url)

        cachedTransaction queryCacheState updateCacheState synchronizeConstellations

    member this.SynchronizeSystem() = 
        let url = Esi.SolarSystems.url
        let synchronizeSystems = synchronizeSystems updateSystems
        let queryCacheState, updateCacheState = this.GetCacheStateOperations(url)

        cachedTransaction queryCacheState updateCacheState synchronizeSystems

    member this.SynchronizeType() = 
        let url = Esi.Types.url
        let synchronizeTypes = synchronizeTypes updateTypes
        let queryCacheState, updateCacheState = this.GetCacheStateOperations(url)

        cachedTransaction queryCacheState updateCacheState synchronizeTypes

    member this.SynchronizeOrder() =
        let synchronizeOrdersInRegion = synchronizeOrdersInRegion updateOrders cleanupOrders
        Order.synchronizeOrders queryRegions synchronizeOrdersInRegion queryCacheState updateCacheState


[<DisallowConcurrentExecution>]
type SynchronizationJob() =
    member this.Do () =
            let stopWatch = Stopwatch.StartNew()
            logger.Information("Starting synchronization")
            let connStringEnvVariable = Environment.GetEnvironmentVariable(ConnStringEnvVariable)
            let settings = { ConnString = connStringEnvVariable; CommandTimeOut = 30; ParralelBatchSize = 8}

            let synchronization = Synchronization(settings)

            let synchronizationResult =
                    synchronization.SynchronizeRegion()
                    |> AsyncResult.bind (fun regionResult -> 
                                            logger.Information("Processed regions {@Result}", regionResult)
                                            synchronization.SynchronizeConstellation())
                    |> AsyncResult.bind (fun constellationResult ->
                                            logger.Information("Processed constellations {@Result}", constellationResult)
                                            synchronization.SynchronizeSystem())
                    |> AsyncResult.bind (fun systemsResult ->
                                            logger.Information("Processed systems {@Result}", systemsResult)
                                            synchronization.SynchronizeType())
                    |> AsyncResult.bind (fun typesResult ->
                                            logger.Information("Processed types {@Result}", typesResult)
                                            synchronization.SynchronizeOrder())
                    |> AsyncResult.bind (fun ordersResult ->
                                            logger.Information("Processed orders {@Result}", ordersResult)
                                            ordersResult |> AsyncResult.wrap)
                    |> Async.RunSynchronously

            stopWatch.Stop()
            logger.Information("Synchronization is complete {Elapsed}: {Result}", stopWatch.Elapsed.TotalSeconds, synchronizationResult)

    interface IJob with
        member this.Execute(ctx: IJobExecutionContext) = Task.Run((fun () -> this.Do()))

let startScheduler() = 

    let props = NameValueCollection()
    props.Item "quartz.serializer.type" <- "json"
    let schedulerFactory = new StdSchedulerFactory(props)
    let scheduler = schedulerFactory.GetScheduler().Result
    scheduler.Start().Wait()
    
    let tmp = 
        JobBuilder.Create<SynchronizationJob>()
            .WithIdentity("SynchronizationJob")
            .Build()

    let trigger = 
        TriggerBuilder.Create()
            .WithIdentity("SynchronizationTrigger")
            .StartAt(DateTimeOffset.UtcNow + TimeSpan.FromSeconds(30.0))
            .WithSimpleSchedule(fun builder -> builder.WithIntervalInMinutes(10).RepeatForever() |> ignore)
            .Build()

    scheduler.ScheduleJob(tmp, trigger).Wait()

    Scheduler <- Some(scheduler)

let webApp =
     GET >=> choose [
        routef "/type/%i" MarketDepthHandlers.getMarketDepthForTypeId
        routef "/type/%s" MarketDepthHandlers.getMarketDepthForTypeName
        route "/types" >=> (warbler (fun _ -> MarketDepthHandlers.convertTypeIdsToTypeNames()))]

let configureApp (app : IApplicationBuilder) =
    app.UseStaticFiles().UseGiraffe webApp
    

let configureServices (services : IServiceCollection) =
    services.AddGiraffe() |> ignore

[<EntryPoint>]
let main _ =
    startScheduler()

    WebHostBuilder()
        .UseKestrel()
        .Configure(Action<IApplicationBuilder> configureApp)
        .ConfigureServices(configureServices)
        .UseUrls([|"http://*:5000"|])
        .Build()
        .Run()
    0