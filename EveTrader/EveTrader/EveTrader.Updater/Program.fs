﻿open System
open System.Diagnostics
open EveTrader.Core.Database
open EveTrader.Core.Logging
open EveTrader.Domain
open EveTrader.Domain.Synchronization
open EveTrader.Utils
type Synchronization(settings: Settings) =
    let queryCacheState = CachedTransaction.queryCacheState settings
    let updateCacheState = CachedTransaction.updateCacheStatus settings
    let updateRegions = Region.updateRegionsInDb settings
    let updateConstellations = Constellation.updateConstellationsInDb settings
    let synchronizeConstellations = Constellation.synchronizeConstellations settings
    let updateSystems = SolarSystem.updateSystemInDb settings
    let synchronizeSystems = SolarSystem.synchronizeSystems settings
    let updateTypes = Type.updateTypeInDb settings
    let synchronizeTypes = Type.synchronizeTypes settings
    let updateOrders = Order.updateOrdersInDb settings
    let cleanupOrders = Order.cleanupOrdersInRegion settings
    let synchronizeOrdersInRegion = Order.synchronizeOrdersInRegion settings
    let queryRegions = Order.queryRegions settings

    let cachedTransaction = CachedTransaction.cachedTransaction SynchronizationError.mapNetworkError

    member private this.GetCacheStateOperations(url: string) =
        (fun () -> queryCacheState url), updateCacheState  url

    member this.SynchronizeRegion() = 
        let url = Esi.Regions.url
        let synchronizeRegions = Region.synchronizeRegions updateRegions
        let queryCacheState, updateCacheState = this.GetCacheStateOperations(url)

        cachedTransaction queryCacheState updateCacheState synchronizeRegions

    member this.SynchronizeConstellation() = 
        let url = Esi.Constellations.url
        let synchronizeConstellations = synchronizeConstellations updateConstellations
        let queryCacheState, updateCacheState = this.GetCacheStateOperations(url)

        cachedTransaction queryCacheState updateCacheState synchronizeConstellations

    member this.SynchronizeSystem() = 
        let url = Esi.SolarSystems.url
        let synchronizeSystems = synchronizeSystems updateSystems
        let queryCacheState, updateCacheState = this.GetCacheStateOperations(url)

        cachedTransaction queryCacheState updateCacheState synchronizeSystems

    member this.SynchronizeType() = 
        let url = Esi.Types.url
        let synchronizeTypes = synchronizeTypes updateTypes
        let queryCacheState, updateCacheState = this.GetCacheStateOperations(url)

        cachedTransaction queryCacheState updateCacheState synchronizeTypes

    member this.SynchronizeOrder() =
        let synchronizeOrdersInRegion = synchronizeOrdersInRegion updateOrders cleanupOrders
        Order.synchronizeOrders queryRegions synchronizeOrdersInRegion queryCacheState updateCacheState

[<EntryPoint>]
let main _ =
    let stopWatch = Stopwatch.StartNew()
    logger.Information("Starting synchronization")
    let connStringEnvVariable = Environment.GetEnvironmentVariable(ConnStringEnvVariable)
    let settings = { ConnString = connStringEnvVariable; CommandTimeOut = 30; ParralelBatchSize = 32}

    let synchronization = Synchronization(settings)

    let synchronizationResult =
            synchronization.SynchronizeRegion()
            |> AsyncResult.bind (fun regionResult -> 
                                    logger.Information("Processed regions {@Result}", regionResult)
                                    synchronization.SynchronizeConstellation())
            |> AsyncResult.bind (fun constellationResult ->
                                    logger.Information("Processed constellations {@Result}", constellationResult)
                                    synchronization.SynchronizeSystem())
            |> AsyncResult.bind (fun systemsResult ->
                                    logger.Information("Processed systems {@Result}", systemsResult)
                                    synchronization.SynchronizeType())
            |> AsyncResult.bind (fun typesResult ->
                                    logger.Information("Processed types {@Result}", typesResult)
                                    synchronization.SynchronizeOrder())
            |> AsyncResult.bind (fun ordersResult ->
                                    logger.Information("Processed orders {@Result}", ordersResult)
                                    ordersResult |> AsyncResult.wrap)
            |> Async.RunSynchronously

    stopWatch.Stop()
    logger.Information("Synchronization is complete {Elapsed}: {Result}", stopWatch.Elapsed.TotalSeconds, synchronizationResult)
    0
