create schema eve_trader
;

alter schema eve_trader owner to postgres
;

set search_path TO eve_trader;


create sequence order_history_id_seq
;

alter sequence order_history_id_seq owner to postgres
;

create table region
(
	region_id integer not null
		constraint region_pkey
			primary key,
	name varchar(24) not null,
	description varchar(256) not null
)
;

alter table region owner to postgres
;

create table type
(
	type_id integer not null
		constraint type_pkey
			primary key,
	name varchar(128) not null,
	published boolean default true not null,
	market_group_id integer,
	volume numeric(16,2),
	packaged_volume numeric(16,2),
	group_id integer not null
)
;

alter table type owner to postgres
;

create table etag
(
	url varchar(512) not null
		constraint etag_pkey
			primary key,
	etag varchar(256) not null,
	updated timestamp default timezone('utc'::text, now()),
	expires timestamp not null
)
;

alter table etag owner to postgres
;

create table constellation
(
	constellation_id integer not null
		constraint constellation_pkey
			primary key,
	region_id integer not null
		constraint constellation_region_region_id_fk
			references region,
	name varchar(256)
)
;

alter table constellation owner to postgres
;

create index constellation_region_id_index
	on constellation (region_id)
;

create table system
(
	system_id integer not null
		constraint system_pkey
			primary key,
	name varchar(256) not null,
	constellation_id integer not null
		constraint system_constellation_constellation_id_fk
			references constellation,
	security_class varchar(16) not null,
	security_status double precision not null
)
;

alter table system owner to postgres
;

create table "order"
(
	order_id bigint not null
		constraint order_pkey
			primary key,
	type_id integer not null
		constraint order_type_type_id_fk
			references type,
	price numeric(15,2) not null,
	is_buy boolean default false not null,
	volume_total bigint not null,
	volume_remaining bigint not null,
	range varchar(16) not null,
	location_id bigint not null,
	system_id integer not null
		constraint order_system_system_id_fk
			references system,
	issued timestamp not null,
	duration interval not null,
	min_volume bigint default 1 not null
)
;

alter table "order" owner to postgres
;

create index order_type_id_index
	on "order" (type_id)
;

create index order_system_id_index
	on "order" (system_id)
;

create index system_constellation_id_index
	on system (constellation_id)
;

create index system_security_status_index
	on system (security_status)
;

create table order_history
(
	id bigserial not null
		constraint order_history_pkey
			primary key,
	order_id bigint not null,
	price numeric(15,2) not null,
	is_buy boolean default false not null,
	volume_total bigint not null,
	volume_remaining bigint not null,
	range varchar(16) not null,
	location_id bigint not null,
	system_id integer not null,
	issued timestamp not null,
	updated timestamp default timezone('utc'::text, now()),
	type_id integer not null,
	active boolean default true not null,
	duration interval not null,
	min_volume bigint default 1 not null
)
;

alter table order_history owner to postgres
;

create index order_history_order_id_index
	on order_history (order_id)
;

create view order_details as
SELECT ord.order_id,
         ord.type_id,
         ord.price,
         ord.is_buy,
         ord.volume_total,
         ord.volume_remaining,
         ord.min_volume,
         ord.range,
         ord.location_id,
         ord.system_id,
         ord.issued,
         ord.duration,
         typ.name,
         sys.security_status,
         sys.name   AS system_name,
         const.name AS constellation_name,
         reg.name   AS region_name,
         reg.region_id
  FROM eve_trader."order" ord,
       eve_trader.type typ,
       eve_trader.region reg,
       eve_trader.constellation const,
       eve_trader.system sys
  WHERE ((ord.type_id = typ.type_id) AND (ord.system_id = sys.system_id) AND
         (sys.constellation_id = const.constellation_id) AND (const.region_id = reg.region_id))
;

alter table order_details owner to postgres
;

